# home_work_1
===1 часть===
Создаем activity на котором будем отображать сверху вниз динамически добавленные компоненты
Создаем toolbar с отображаемым в нем menu (создаем именно androidx.appcompat.widget.Toolbar), 
меню отображаем используя xml menu
В меню должно быть три пункта:
  создать кнопку, 
  создать поле для ввода текста (очевидно первый добавляет кнопку второй добавляет поле для ввода текста), 
  третий пункт назовем очистить (очевидно он удаляет созданные элементы).
В портретной ориентации цвет кнопки должен быть серый и в лендскейпной ориентации красный.
Текстовое поле создаем программно, кнопку с помощью LayoutInflater-а
Текст на создаваемой кнопке должен быть 
  для русской локали "Создать элементы" 
  для англ локали "Create elements"

- вьюшки добавляются в конец, вьюшки должны иметь высоту по размеру контента и ширину на весь экран
- в логах пишем информацию о созданной вьюшке (в произвольной форме, например %%button created%% %%text created%%
- элементов может быть много, нужно чтобы можно было скролить получившийся результат

===2 часть===  
Для динамического создания элементов введем такие понятия как
"text" - текстовое поле
"button" - кнопка

сделать так чтобы при нажатии на любую созданную кнопку, 
приложение смотрело на текстовые поля, чмитало введенные значения 
и ля каждого поля для ввода текста создавало соот-ие элементы 
а для ошибочно введенных возвращало ошибки тоастом. 
Например:
На экране три поля для ввода текста и не важно сколько кнопок. 
  В первом поле написано "text" во втором написан "button" 
  в третьем какая-нибудь фигня но не "text" и не "button" 
При нажатии на любую из кнопок, имеющихся на экране, нужно 
  добавть одно текстовое поле, 
   одну кнопку 
   и вывести toast сообщение об ошибке
                               
- меняем информацию выводимую в лог. выводим кол-во созданных button и text (для создаваемых через меню такой же лог, то есть там всегда будет по одному или кнопка или текст)
- если нет ни одного поля для ввода текста и нажали на кнопку создания элементов, то выводим toast с текстом %%Нужно хотя бы одно поле для ввода текста%% (или типо того, не суть важно)
- после нажатия на кнопку все edittext должны снова стать пустыми

===3 часть===
Добавить float button по нажатию на которую будем показывать диалог (используйте DialogFragment) в котором покажем пользователю текстовое поле и кнопку ok. По нажатию на кнопку диалог закрывается и если пользователь вводит button или text то работаем по старой схеме, добавляем кнопку или текст, если ввел фигню то показываем ошибку.
Давайте дадим возможность пользователю ввести несколько элементов сразу, а именно: указывать элементы через запятую. Например

"text" - одно текстовое поле
"text,text,button" - два текстовых поля, две кнопки
Так же считаем валидными вводы когда вначале и в конце может быть пробел, то есть "_text_" ("_" - тут символ пробела). То есть так же валидным будет написать "_text,text,___button,button__"

- Кнопка это элемент разметки приложения, то есть она как бы всегда присутствует в layout-е
- Кнопка прижата книзу справа, при скроле вниз убирается при скроле вверх показывается
