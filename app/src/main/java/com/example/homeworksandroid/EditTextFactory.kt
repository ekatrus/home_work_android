package com.example.homeworksandroid

import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout

class EditTextFactory : ElementsFactory() {
    override fun create(parent: ViewGroup): EditText {
        val context = parent.context
        val edtText = EditText(context)
        edtText.apply {
            layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            edtText.setHint(R.string.edit_text_hint)
            return this
        }
    }
}