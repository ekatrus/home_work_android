package com.example.homeworksandroid

import android.view.View
import android.view.ViewGroup

abstract class ElementsFactory {
    abstract fun create(parent: ViewGroup): View
}