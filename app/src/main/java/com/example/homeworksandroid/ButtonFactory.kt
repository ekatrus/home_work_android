package com.example.homeworksandroid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class ButtonFactory (private val clickListener: View.OnClickListener) : ElementsFactory() {
    override fun create(parent: ViewGroup): View {
        val layoutInflater = LayoutInflater.from(parent.context)
        val button = layoutInflater.inflate(R.layout.button, parent, false)
        button.setOnClickListener(clickListener)
        return button
    }
}