package com.example.homeworksandroid

import android.app.Dialog
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class AddDialogFragment : DialogFragment() {

    private var mOnInputListener: OnInputListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val input = EditText(context as MainActivity)
        input.setHint(R.string.edit_text_hint)
        return AlertDialog.Builder(context as MainActivity)
            .setView(input)
            .setPositiveButton("OK") { _, _ -> mOnInputListener?.onClickDialogPositiveButton(input.text.toString()) }
            .create()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mOnInputListener = activity as OnInputListener?
        } catch (e: ClassCastException) {
            Log.e(TAG, "onAttach: ClassCastException: " + e.message)
        }
    }

    interface OnInputListener {
        fun onClickDialogPositiveButton(input: String)
    }
}