package com.example.homeworksandroid

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.get
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity(), View.OnClickListener, AddDialogFragment.OnInputListener {
    private val layout by lazy { findViewById<LinearLayout>(R.id.layout) }

    override fun onClickDialogPositiveButton(input: String) {
        onClickToCreateElements(input, layout, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fabAdd = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        fabAdd.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.manage_ellement_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.add_button -> {
            layout.addView(ButtonFactory(this).create(layout))
            Log.d("TAG", "button created")
            true
        }

        R.id.add_textView -> {
            layout.addView(EditTextFactory().create(layout))
            Log.d("TAG", "text created")
            true
        }

        R.id.clear -> {
            layout.removeAllViews()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onClick(v: View) {

        when (v.id) {
            R.id.floatingActionButton -> AddDialogFragment().show(this.supportFragmentManager, "AddDialog")
            else -> onClickToCreateElement(layout, this)
        }
    }
}


private fun onClickToCreateElement(parent: ViewGroup, clickListener: View.OnClickListener) {
    var countTextView = 0
    var countWordButton = 0
    var countWordText = 0
    for (i in 0 until parent.childCount) {
        if (parent[i] is EditText) {
            countTextView += 1

            when ((parent[i] as EditText).text.toString()) {
                "text" -> {
                    parent.addView(EditTextFactory().create(parent))
                    countWordText += 1
                }
                "button" -> {
                    parent.addView(ButtonFactory(clickListener).create(parent))
                    countWordButton += 1
                }
                else -> showToast(parent.context, R.string.nothing_to_create)
            }
            (parent[i] as EditText).text.clear()
        }
    }
    if (countTextView == 0) {
        showToast(parent.context, R.string.textView_dont_exist)
    } else {
        Log.d("TAG", "$countWordButton button and $countWordText textView was created")
    }
}


private fun onClickToCreateElements(editText: String, parent: ViewGroup, clickListener: View.OnClickListener) {
    val textInEdtText =
        editText.replace(" ", "").toLowerCase().split(",")

    val countWordTextInTextView = textInEdtText.filter { it == "text" }.size
    val countWordButtonInTextView = textInEdtText.filter { it == "button" }.size

    val incorrectWord = textInEdtText.size - countWordTextInTextView - countWordButtonInTextView

    if (textInEdtText.isNotEmpty() && incorrectWord != textInEdtText.size) {
        repeat(countWordTextInTextView) {
            parent.addView(EditTextFactory().create(parent))
        }
        repeat(countWordButtonInTextView) {
            parent.addView(ButtonFactory(clickListener).create(parent))
        }

        if (incorrectWord > 0) showToast(parent.context, R.string.did_mistake)
    } else {
        showToast(parent.context, R.string.nothing_to_create)
    }

    Log.d("TAG", "$countWordButtonInTextView button and $countWordTextInTextView textView was created from dialog")
}

private fun showToast(context: Context, text: Int) {
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}